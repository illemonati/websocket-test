import asyncio
import websockets
import sys

LHOST = '0.0.0.0'
LPORT = 5566


async def server(websocket, path):

    loop = asyncio.get_event_loop()

    async def handle_send():
        message = sys.stdin.readline()
        await websocket.send(message)
        print(1)

    def handle_input():
        print(3)
        loop.create_task(handle_send())
        print(2)

    async def handle_recv():
        async for message in websocket:
            print(message)
            await asyncio.sleep(0)

    recv = asyncio.create_task(handle_recv())
    loop.add_reader(sys.stdin, handle_input)
    await recv


asyncio.get_event_loop().run_until_complete(
    websockets.serve(server, LHOST, LPORT))

asyncio.get_event_loop().run_forever()
